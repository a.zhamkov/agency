<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информация");
?>
<section class="property-section"
		 style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/background/1.jpg);">
	<div class="auto-container">
		<div class="sec-title light text-center">
			<div class="devider"><span></span></div>
			<h2><?$APPLICATION->SetTitle(false);?></h2>
			<div class="text">Информация о компании</div>
		</div>

	</div>
</section>
<section class="services-section">
	<div class="auto-container">
		<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
			"",
			Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "inc",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/includes/info.php"
			)
		);?>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
