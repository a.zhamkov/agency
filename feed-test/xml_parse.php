<?php
$_SERVER["DOCUMENT_ROOT"] = "/var/www/u1381083/data/www/u1381083.isp.regruhosting.ru";
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");


require_once 'helper.php';
require_once 'FeedObj.php';
require_once 'PropListObj.php';
require_once 'do_echo_class.php';

CModule::IncludeModule('iblock');

$IBLOCK_ID = 2;

class PropObj {
    private $name;
    private $type;

    public function __construct($name, $type)
    {
        $this->name = $name;
        $this->type = $type;
    }

    public function getType() {
        return $this->type;
    }
    public function getName() {
        return $this->name;
    }
}

class BXElement {
    /**
     * @var array
     */
    private $data;
    private $iblockId;


    public function __construct($iblockId, $obj) {
        $this->iblockId = $iblockId;
        $this->data = $obj;
    }

    public function creatOrUpdateElement($iblockId) {
        $name = self::getElementName($this->data);
//        echo '<pre>';
//        print_r($name);
//        echo '</pre>';

//        foreach ($this->data as $c_prop => $item) {
//            /** @var PropObj $p */
//            $p = $item['prop'];
//            return $p->getName();
//        }

        $el = new CIBlockElement;

        $arLoadProductArray = self::getLoadProductArray($iblockId, $this->data);

        $PRODUCT_ID = $this->getProductId($iblockId, $arLoadProductArray['CODE']);
//        echo '$arLoadProductArray';
//        echo '<pre>';
//        print_r($arLoadProductArray);
//        echo '</pre>';

        if($PRODUCT_ID) {
            unset($arLoadProductArray["IBLOCK_ID"]);
            CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, $iblockId, array('7' => Array ("VALUE" => array("del" => "Y"))));

            $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
            return "UPdate ID: " . $arLoadProductArray["CODE"] .' - ' . $name .'<br/>';
        } else {
            if ($res = $el->Add($arLoadProductArray)) {
                return "New ID: " . $res .' - ' . $name .'<br/>';
            }else {
                return "Error: " . $el->LAST_ERROR .' - ' . $name .'<br/>';
            }
        }
    }

    public function getProductId($iblockId, $productCode) {
        return CIBlockFindTools::GetElementID(false, $productCode, false, false, array("IBLOCK_ID" => $iblockId));
    }

    public function getElementName($obj) {
//        echo '$name obj';
//        echo '<pre>';
//        print_r($obj['AREA']);
//        echo '</pre>';
        $strLoc = $obj['AREA']['value']['value'] . ' м2, '
            . $obj['LOCATION__ADDRESS']['value'] . ', '
            . $obj['LOCATION__LOCALITY_NAME']['value'];
//        echo '$strLoc' . $strLoc;

//        echo '<pre>';
//        print_r($obj['_INTERNAL_ID']);
//        echo '</pre>';
        switch ($obj['CATEGORY']['value']) {
            case "flat":
                if($obj['ROOMS']['value']['value']){
                    $strRooms = $obj['ROOMS']['value']['value'] . '-комн.' .' квартира,';
                    return $strRooms . $strLoc;
                } else if(!empty($obj['STUDIO']['value']['value']) && $obj['ROOMS']['value']['value'] > 0) {
                    $strStudio = 'квартира-студия ';
                    return $strStudio . $strLoc;
                } else {
                    return $strLoc;
                }
                break;
            default:
                return $strLoc;
        }
    }

    public function addPropTypeList($iblockId, $propCode, $propValue) {
        $ENUM_ID = 0;
        $property_enums = CIBlockPropertyEnum::GetList(
            Array(),
            Array("IBLOCK_ID"=>$iblockId, "CODE"=>$propCode,  "XML_ID" => $propValue)
        );
        while($enum_fields = $property_enums->GetNext())
        {
            $ENUM_ID =  $enum_fields["ID"];
//        echo 'enum - ' . $ENUM_ID;
        }
        return $ENUM_ID;
    }

    public function getPropValue($iblockId, $prop) {
//        echo '<pre>';
//        print_r($prop);
//        echo '</pre>';
        $prop_value = $prop['value'];
        $prop_code = $prop['code'];
        /** @var PropObj $cur_prop */
        $cur_prop = $prop['prop'];
        $prop_name = $cur_prop->getName();
        $prop_type = $cur_prop->getType();

//        echo ' $prop_value - ' . $prop_value;
//        echo PHP_EOL;
//        echo  '1' . $prop_code . ' $prop_value - ' . $prop_value .'<br/>';
        if($prop_value) {
            switch ($prop_type) {
                case "N": // свойство NUMBER
                    $PROP[$prop_code] = $prop_value['value'];
//                    echo $prop_code . ' $prop - ' . $PROP[$prop_code]. '</br>';
                    return $PROP[$prop_code];
                    break;
                case "L": // LIST
                    $ENUM_ID = self::addPropTypeList($iblockId, $prop_code, $prop_value);
                    $PROP[$prop_code] = array("VALUE" => $ENUM_ID);
//                    echo $prop_code . ' $prop - ' . $PROP[$prop_code]['VALUE'] . ' $ENUM_ID - ' . $ENUM_ID . '</br>';
                    return $PROP[$prop_code];
                    break;
                case "S": // STRING
                    $PROP[$prop_code] = $prop_value;
//                    echo $prop_code . '$prop - ' . $PROP[$prop_code]. '</br>';
                    return $PROP[$prop_code];
                    break;
                case "F": // свойство файл
                    $PROP[$prop_code] = '';
                    $PROP[$prop_code] = self::addPropTypeFile($prop_value);
//                    echo $prop_code . ' $prop - ' . $PROP[$prop_code]. '</br>';
                    return $PROP[$prop_code];
                    break;
                default:
                    return null;
                    break;
//                    echo 'ошибка нет собвадений типа для свойства' .'<br/>';
            }
        } else
            return null;
    }

    public function addPropTypeFile (array $images) {
        // ****** MORE_PHOTO*******
        $rsMorePhoto = [];
        $i = 0;
        foreach ($images as $image) {
            $rsMorePhoto['n'.$i] = array("VALUE"=>CFile::MakeFileArray($image));
            $i++;
        }
        // *************************
        return $rsMorePhoto;
    }

    public function getLoadProductArray($iblock, $obj) {
        $PROP = [];
        foreach ($obj as $propItem) {
//            echo '<pre>';
//            print_r($propItem);
//            echo '</pre>';
            $code = $propItem['code'];
            $PROP[$code] = self::getPropValue($iblock, $propItem);
        };

        return Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID" => $iblock,
            "NAME" => self::getElementName($obj),
            "CODE" => $obj['_INTERNAL_ID']['value'],
            "ACTIVE" => "Y",
            "PROPERTY_VALUES" => $PROP,
            "DETAIL_TEXT" => $obj['DESCRIPTION']['value'],
        );
    }
}

do_echo("Start\n");


$propListOne = Helper::propsToList($IBLOCK_ID);

$propListObj = new PropListObj($propListOne);

$start = 'REALTY_FEED__OFFER';

$obj = new FeedObj("feed_u1381083.xml", $start);

$offer_data = $obj->getData($propListObj);
//echo '<pre>';
//echo 'количествео объектов - ';
//print_r(count($offer_data));
//echo '</pre>';
do_echo("start -\n");
//
$count = 0;
$countAll = 0;
foreach ($offer_data as $element) {
//    echo 'element';
//    echo '<pre>';
//    print_r($element);
//    echo '</pre>';
    $el = new BXElement($IBLOCK_ID, $element);
    $temp = $el->creatOrUpdateElement($IBLOCK_ID);
    if ($countAll % 20 == 0) {
        $countAll = $countAll + 20;
        do_echo("count - $countAll\n");
        do_echo ("result: $temp\n");
        $count = 0;
    }
    $countAll++;
}


exit();
?>


