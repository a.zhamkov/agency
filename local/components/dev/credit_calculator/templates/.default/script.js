// $(document).ready(function() {
    let totalCost; //общая сумма
    let firstRate; //первоначальный взнос
    let percentRate; // процентная ставка
    let yearsIpo;   // срок ипотеки лет
    let monthsIpo; //срок ипотеки месяцев
    let monthPay;   // ежемесячный платеж
    let typeRate;   // тип платежа
    let sumIpotek;  // сумма ипотеки
    let monthRate; // ежемесячная ставка
    let allRate; // общая ставка
    let overpayment; // переплата
    let amortization; // основная часть
    let percentage; // процентная часть
    let remainderSum; // остаток долга
    let percentOverpayment; // процент переплаты

    $('#ipo input[type=text]').bind("change keyup input click", function() {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
    });
    totalCost = $('#ipo input#total-cost').val();
    firstRate = $('#ipo input#first-rate').val();
    percentRate = $('#ipo output[name=percentRateOutput]').text();
    yearsIpo = $('#ipo output[name=yearsIpoOutput]').text();
    typeRate = $('input:checked').val();
    // console.log (totalCost +  '; '  + firstRate + '; ' + percentRate + '; ' + yearsIpo + '; ' + typeRate + '; ')

    $('#ipo input#total-cost').change(function () {
        totalCost = $(this).val();
    });
    $('#ipo input#first-rate').change(function () {
        firstRate = $(this).val();
    });
    $('#ipo input[name=percentRateInput]').change(function () {
        percentRate = $('output[name=percentRateOutput]').text();
        console.log(percentRate);
    });
    $('#ipo input[name=yearsIpoInput]').change(function () {
        yearsIpo = $('output[name=yearsIpoOutput]').text();
        console.log(yearsIpo);
    });
    $('#ipo input[name=type-payment]').on('click', function () {
        typeRate = $('input:checked').val();
        //alert(res);
    });

    $('#resultCalc').on('click', function () {
        console.log('total - ' + totalCost);
        console.log('firstRate - ' + firstRate);
        if(totalCost > firstRate) {
            sumIpotek = totalCost - firstRate; // сумма кредита
            monthRate = parseFloat((percentRate / 12 / 100).toFixed(4)); // ежемесячная ставка
            // console.log ('ежемесяч ставка - ' + monthRate);
            monthsIpo = yearsIpo * 12; // срок кредита в месяцах
            // console.log ('срок в мес - ' + monthsIpo);
            allRate = Math.pow((1 + monthRate), monthsIpo); // общая ставка
            // console.log ('Общая ставка - ' + allRate);
            switch (typeRate) {
                case 'ann':
                    monthPay = (sumIpotek * monthRate * allRate / (allRate - 1)).toFixed(0); // ежемесячный платеж
                    overpayment = monthPay * monthsIpo - sumIpotek;
                    percentOverpayment = parseFloat((overpayment / sumIpotek * 100).toFixed(0));

                    $('#month-pay').html('<h5>' + monthPay + ' руб.</h5>');
                    $('#overpayment').html('<h5>' + overpayment + ' руб. </h5> (' + percentOverpayment + '% от суммы кредита)');

                    break;
                case 'diff':
                    amortization = parseFloat((sumIpotek / monthsIpo).toFixed(1)); // основная часть
                    percentage = parseFloat((sumIpotek * monthRate).toFixed(1)); // процентная часть
                    let monthPayFirst = parseFloat((amortization + percentage).toFixed(2)); // первый платёж
                    remainderSum = sumIpotek;
                    let $i, // счетчик
                        monthPayLast; // последний ежемесячный платёж

                    overpayment = 0;
                    monthPay = 0;
                    for ($i = 1; $i <= monthsIpo; $i++) {
                        percentage = remainderSum * monthRate; // процентная часть
                        remainderSum = remainderSum - amortization; // остаток долга
                        monthPay = percentage + amortization; // ежемесячный платеж
                        monthPayLast = parseFloat(monthPay.toFixed(2)); // последний ежемесячный платёж
                        overpayment = parseFloat((overpayment + monthPay).toFixed(2)); // переплата
                    }
                    overpayment = parseFloat((overpayment - sumIpotek).toFixed(2));
                    percentOverpayment = parseFloat((overpayment / sumIpotek * 100).toFixed(0));
                    $('#month-pay').html('от <h5>' + monthPayFirst + ' руб.</h5>' + ' до ' + '<h5>' + monthPayLast + ' руб.</h5>');
                    $('#overpayment').html('<h5>' + overpayment + ' руб. </h5> (' + percentOverpayment + '% от суммы кредита)');
                    break;
                default:
                    alert('Не выбран Тип платежей');
            }
            $('#sum-ipotek').html('<h5>' + sumIpotek + ' руб.</h5>');
        } else {
            alert('Первоначальный взнос больше суммы кредита.');
        }
        // console.log ('ежем платеж - ' + monthPay);


    });

// });
