<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
echo $arResult['DATE']; ?>

<!-- GOOGLE MAP & CONTACT FORM -->
<div id="ipo" class="section-content">
    <div class="contact-form p-a30 bg-white">
        <div class="contact-one">
            <!-- TITLE START -->
            <div class="section-head text-left">
                <h3 class="m-b5">Калькулятор ипотеки</h3>
            </div>
            <!-- TITLE END -->
            <div class="row">
                <div class="col-md-4 p-b15" data-mh="calc-group1">
                    <div class="form-group" >
                        <h4>Стоимость, руб.</h4>
                        <input id="total-cost" name="total-cost" type="text"
                               class="form-control" required placeholder="2000000"
                               value="<?=(is_numeric($arParams['TOTAL_COST'])?$arParams['TOTAL_COST']:'2000000')?>">
                    </div>
                </div>
                <div class="col-md-4 p-b15" data-mh="calc-group1">
                    <div class="form-group">
                        <h4>Срок ипотеки, лет</h4>
                        <!--                                    <span id="slider_years-ipo">10 - 25</span>-->
                        <form oninput="yearsIpoOutput.value = parseInt(yearsIpoInput.value)">
                            <datalist id="tickmarks">
                                <option value="5" label="5">
                                <option value="10" label="10">
                                <option value="15" label="15">
                                <option value="20" label="20">
                                <option value="25" label="25">
                            </datalist>
                            <input name="yearsIpoInput" id="years-ipo_input" type="range" list="tickmarks"
                                   min="<?=(is_numeric($arParams['TERM_START']))?$arParams['TERM_START']:'5'?>"
                                   max="<?=(is_numeric($arParams['TERM_END']))?$arParams['TERM_END']:'25'?>"
                                   step="<?=(is_numeric($arParams['TERM_STEP']))?$arParams['TERM_STEP']:'1'?>"
                                    value="20">
                            <output for="years-ipo_input" name="yearsIpoOutput">20</output>

                        </form>
                    </div>

                </div>

                <div class="col-md-4 p-b15" data-mh="calc-group1">
                    <div class="form-group">
                        <h4>Тип платежа</h4>
                        <div class="form-check">
                            <input name="type-payment" type="radio"
                                   class="form-check-input" id="type-payment-ann" value="ann" checked="checked">
                            <label class="form-check-label" for="type-payment-ann">
                                Аннуитетные платежи
                            </label>
                            <input name="type-payment" type="radio"
                                   class="form-check-input" id="type-payment-diff" value="diff">
                            <label class="form-check-label" for="type-payment-diff">
                                Дифференцированные платежи
                            </label>
                        </div>
                    </div>

                </div>

                <div class="col-md-4 p-b15" data-mh="calc-group2">
                    <div class="form-group" >
                        <h4>Первоначальный взнос, руб</h4>
                        <input id="first-rate" name="first-rate" type="text"
                               class="form-control" required placeholder="200000" value="<?=(is_numeric($arParams['TOTAL_COST'])?($arParams['TOTAL_COST']* (10 / 100)):'200000')?>">
                    </div>
                </div>
                <div class="col-md-4 p-b15" data-mh="calc-group2">
                    <div class="form-group">
                        <h4>Процентная ставка, % годовых</h4>
                        <!--                                    <span id="slider_pr_rate">5 - 15</span>-->
                        <form oninput="percentRateOutput.value = parseFloat(percentRateInput.value)">
                            <datalist id="tickmarksPers">
                                <option value="5" label="5">
                                <option value="10" label="10">
                                <option value="15" label="15">
                            </datalist>
                            <input name="percentRateInput" id="percent-rate" type="range" list="tickmarksPers"
                                   min="<?=(is_numeric($arParams['PERCENT_RATE_START']))?$arParams['PERCENT_RATE_START']:'5'?>"
                                   max="<?=(is_numeric($arParams['PERCENT_RATE_END']))?$arParams['PERCENT_RATE_END']:'20'?>"
                                   step="<?=(is_numeric($arParams['PERCENT_RATE_STEP']))?$arParams['PERCENT_RATE_STEP']:'0.1'?>"
                                    value="6">
                            <output for="percent-rate" name="percentRateOutput">6</output>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 p-b15 align-self-end" data-mh="calc-group2">
                    <div class="ipo-button-block">
                        <div class="site-button radius-no text-uppercase font-weight-600 btn btn-primary"
                             id="resultCalc">Расчитать
                        </div>
                    </div>

                </div>
            </div>
            <div class="row result-block" >
                <div class="col-md-3">
                    <div class="form-group">
                        <div>
                            <h5>Сумма кредита</h5>
                            <span id="sum-ipotek"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div>
                            <h5>Ежемесячный платёж</h5>
                            <span id="month-pay"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <div>
                            <h5>Переплата</h5>
                            <span id="overpayment"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <? if ($arParams['CHECKBOX'] == 'y') { ?>
            <form class="cons-contact-form" method="post"
                  action="">
                <div class="contact-one">

                    <!-- TITLE START -->
                    <div class="section-head text-left">
                        <h3 class="m-b5">Ваши контакты</h3>
                    </div>
                    <!-- TITLE END -->
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input name="username" type="text" required class="form-control" required
                                       placeholder="Имя">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input name="email" type="text" class="form-control" required placeholder="Email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input name="phone" type="text" class="form-control" required placeholder="Телефон">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="text-right">
                                <button name="submit" type="submit" value="Submit"
                                        class="site-button radius-no text-uppercase font-weight-600" type="button">
                                    Отправить
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        <? } ?>
    </div>
</div>

<?//='<pre>'?>
<?//print_r($arParams);?>
<?//='</pre>'?>
