<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    "GROUPS" => array(
        "FORM" => array(
            "NAME" => "Форма",
            "SORT" => "300",
        ),
        "TERM" => array(
            "NAME" => "Срок ипотеки, лет",
        ),
        "PERCENT_RATE" => array(
            "NAME" => "Процентная ставка, %",
        ),
        "TOTAL_COST" => array(
            "NAME" => "Стоимость объекта"
)
    ),
    "PARAMETERS" => array(
        "CHECKBOX" => array(
            "PARENT" => "FORM",
            "NAME" => "Показывать форму",
            "TYPE" => "CHECKBOX",
        ),
            "TERM_START" => array(
                "PARENT" => "TERM",
                "NAME" => "от",
                "TYPE" => "STRING",
            ),
            "TERM_END" => array(
                "PARENT" => "TERM",
                "NAME" => "до",
                "TYPE" => "STRING",
            ),
            "TERM_STEP" => array(
                "PARENT" => "TERM",
                "NAME" => "Шаг шкалы",
                "TYPE" => "STRING",
            ),
            "PERCENT_RATE_START" => array(
                "PARENT" => "PERCENT_RATE",
                "NAME" => "от",
                "TYPE" => "STRING",
            ),
            "PERCENT_RATE_END" => array(
                "PARENT" => "PERCENT_RATE",
                "NAME" => "до",
                "TYPE" => "STRING",
            ),
            "PERCENT_RATE_STEP" => array(
                "PARENT" => "PERCENT_RATE",
                "NAME" => "Шаг шкалы",
                "TYPE" => "STRING",
            ),
        "TOTAL_COST" => array (
            "PARENT" => "TOTAL_COST",
            "NAME" => "Стоимость объекта",
            "TYPE" => "STRING"
        )
    ),
);
