$('#most-objects #detail-pict').owlCarousel({
    items:1,
    loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:3
        }
    }
});
$('.image-box .owl-carousel').owlCarousel({
    items:1,
    loop:false,
    margin:0,
    autoplay: true,
    mouseDrag : false,
    touchDrag : false,
});
