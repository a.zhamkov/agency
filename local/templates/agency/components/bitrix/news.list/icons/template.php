<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="feature-block-two col-lg-6 col-md-6 col-sm-12">
        <div class="inner-box">
            <span class="icon flaticon-<?=$arItem['PROPERTIES']['ICON']['VALUE_XML_ID']?>"></span>
            <h4><a href="about.html"><?=$arItem['PROPERTIES']['TITLE']['VALUE']?></a></h4>
            <div class="text"><?=$arItem['PROPERTIES']['SUBTITLE']['VALUE']?></div>
        </div>
    </div>
<?endforeach;?>
