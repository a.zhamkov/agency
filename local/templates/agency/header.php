<?//
//if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
//    die();
//?>
<!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <? $APPLICATION->ShowHead(); ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon-32х32.png" type="image/x-icon">
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH ?>/images/favicon-32х32.png" type="image/x-icon">

    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/bootstrap.css"); ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/style.css"); ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/responsive.css"); ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/color-switcher-design.css"); ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/color-themes/default-theme.css"); ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/sass/custom.css"); ?>

    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/respond.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/popper.min.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery-ui.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.fancybox.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/owl.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/wow.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.matchheight.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/isotope.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.stellar.min.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/appear.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/script.js"); ?>
    <? $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/color-settings.js"); ?>



    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
    <![endif]-->


</head>
<body>
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>


<div class="page-wrapper">
    <!-- Preloader -->
<!--    <div class="preloader"></div>-->

    <!-- Main Header-->
    <header class="main-header header-style-one">
        <!--Header Top-->
        <div class="header-top">
            <div class="auto-container">
                <div class="inner-container clearfix">
                    <div class="top-left">
                        <ul class="contact-list clearfix">
                            <li><i class="fa fa-phone-volume"></i>
                                <a id="phone_header" href="">
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/includes/text_block/phone.php"
                                        )
                                    );?>
                                </a></li>
                            <li><i class="fa fa-envelope"></i><a id="email_header" href=""><?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/includes/email.php"
                                        )
                                    );?></a></li>
                        </ul>
                    </div>
                    <div class="top-right clearfix">
                        <ul class="social-icon-one">
                            <li><a href=""><span class="fab fa-facebook-f"></span></a></li>
                            <li><a href=""><span class="fab fa-twitter"></span></a></li>
                            <li><a href=""><span class="fab fa-skype"></span></a></li>
                            <li><a href=""><span class="fab fa-linkedin-in"></span></a></li>
                        </ul>

<!--                        <ul class="login-signup">-->
<!--                            <li><a href="index.html#">Login</a></li>-->
<!--                            <li><a href="index.html#">SignUp</a></li>-->
<!--                        </ul>-->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Top -->
        <script>
            function setHrefOnHomePage($selector) {
                $selector = '#'+$selector;
                let tmp = $($selector).text();
                tmp = $.trim(tmp);
                $($selector).attr('href', tmp);
            }
            $(document).ready(function () {
                let $email = $('#email_header').text();
                $email = $.trim('mailto:'+$email);
                $('#email_header').attr('href', $email);
            })
        </script>
        <!--Header Lower-->
        <div class="header-lower">
            <div class="auto-container">
                <div class="main-box clearfix">
                    <div class="pull-left logo-outer">
                        <div class="logo">
                            <a href="/">
                                <img src="<?= SITE_TEMPLATE_PATH ?>/images/icons/empty.png"
                                data-src="<?= SITE_TEMPLATE_PATH ?>/images/logo_b1_meddium.png" alt="everest24" title="everest24">
                            </a>
                        </div>
                    </div>

                    <!--Nav Box-->
                    <div class="nav-outer clearfix">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>

                        <!-- Main Menu -->
                        <nav class="main-menu navbar-expand-lg navbar-light">
                            <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="current dropdown"><a href="/">Главная</a>
                                    </li>
                                    <li class="dropdown has-mega-menu"><a href="/catalog/">Каталог</a>
                                    </li>
                                    <li><a href="/informatsiya/">Информация</a></li>
<!--                                    <li><a href="#">Новости</a></li>-->
                                    <li><a href="/contacts/">Контакты</a></li>

                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                        <div class="outer-box">
                            <!-- Btn Box -->
                            <div class="btn-box">
                                <a href="#"
                                   class="theme-btn btn-style-one">
                                    <span class="btn-title">Заказ звонка</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Header Lower-->

        <!-- Sticky Header  -->
        <div class="sticky-header">
            <div class="auto-container clearfix">
                <!--Logo-->
                <div class="logo pull-left">
                    <a href="/" title="">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/icons/empty.png"
                             data-src="<?= SITE_TEMPLATE_PATH ?>/images/logo_b1_meddium.png" alt="everest24" title="everest24"></a>
                </div>
                <!--Right Col-->
                <div class="nav-outer pull-right">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>

                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-lg">
                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix"><!--Keep This Empty / Menu will come through Javascript--></ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div><!-- End Sticky Menu -->


        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel-1"></span></div>

            <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
            <nav class="menu-box">
                <div class="nav-logo">
                    <a href="/">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/images/icons/empty.png"
                             data-src="<?= SITE_TEMPLATE_PATH ?>/images/logo.svg" alt="" title="">
                    </a>
                </div>

                <ul class="navigation clearfix"><!--Keep This Empty / Menu will come through Javascript--></ul>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!--End Main Header -->
    <? if (CSite::InDir('/index.php')): ?>
    <!-- Banner Section -->
    <section class="banner-section-two">
        <div class="banner-carousel-two owl-carousel owl-theme">
            <div class="slide-item"
                 data-mh="slider-group"
                 style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/images/main-slider/2.jpg);">
                <div class="auto-container">

                    <div class="content-box clearfix">
                        <div class="left-content">
                            <div class="status"><span>Успешное агентство</span></div>
                            <h2><?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => "/includes/slider/slider1_title.php"
                                    )
                                );?></h2>
                            <div class="text">
                                <ul>
                                    <?$APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        "",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/includes/slider/slider1_list.php"
                                        )
                                    );?>
                                </ul>
                            </div>
                            <div class="btn-box">
                                <a href="/catalog/" class="theme-btn btn-style-two">
                                    <span class="btn-title"><?$APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            "",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => "/includes/slider/slider1_btn.php"
                                            )
                                        );?></span></a></div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Slide Item -->
            <div class="slide-item"
                 data-mh="slider-group"
                 style="background-image: url(
                 <?= SITE_TEMPLATE_PATH ?>/images/main-slider/3.jpg);">
                <div class="auto-container">

                    <div class="content-box clearfix">
                        <div class="left-content">
                            <div class="status"><span>Успешное агентство</span></div>
                            <h2>
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => "/includes/slider/slider2_title.php"
                                    )
                                );?>
                            </h2>
                            <div class="text">
                                <?$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "EDIT_TEMPLATE" => "",
                                        "PATH" => "/includes/slider/slider2_list.php"
                                    )
                                );?>
                            </div>
                            <div class="btn-box">
                                <a href="#" class="theme-btn btn-style-two">
                                    <span class="btn-title">
                                        <?$APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            "",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => "/includes/slider/slider2_btn.php"
                                            )
                                        );?>
                                    </span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!--End Banner Section -->
    <?endif;?>

