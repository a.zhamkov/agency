<?
use Cetacs\Board_Core\CEnmRealtyCategory;
use Cetacs\Board_Core\CAllLists;
use Cetacs\Board_Core\CLstPriceCurrency;
use Cetacs\Board_Core\CEnmDealType;

class CPhrases{
    public static function GetRandomPhrase($objRealty){
        $arStorage = self::GetStorage($objRealty);

//        $idx = rand(0, count($arStorage)-1);
        $idx = $objRealty->nId % count($arStorage);


        return $arStorage[$idx];
        //return $arStorage[5];
    }

    /**
     * @param \Cetacs\Board_Core\CRealty $objRealty
     * @return array
     */
    private static function GetStorage($objRealty){
        
        switch ($objRealty->enmRealtyCategory){
	    case CEnmRealtyCategory::FLAT: $strRealtyType = "квартиры"; $strMore = "квартире"; $strMore2 = "квартиру"; break;
	    case CEnmRealtyCategory::ROOM: $strRealtyType = "комнаты"; $strMore = "комнате"; break;
	    case CEnmRealtyCategory::HOUSE: $strRealtyType = "дома"; $strMore = "доме"; break;
	    case CEnmRealtyCategory::TOWNHOUSE: $strRealtyType = "таунхауса"; $strMore = "таунхаусе"; break;
	    case CEnmRealtyCategory::LAND: $strRealtyType = "участка"; $strMore = "участке"; break;
	    case CEnmRealtyCategory::DEPOT: $strRealtyType = "склада"; $strMore = "складе"; break;
	    case CEnmRealtyCategory::OFFICE: $strRealtyType = "офиса"; $strMore = "офисе"; break;
	    default: $strMore = ""; $strRealtyType ="";
	}
	

        //ЗАГОЛОВОК
        $strTitle = "";
        $strTitle_emptytxt = "";
        $strTitle_numroom = "";
        $strTitle_numroomPIf = "";
        $strTitle_numroomPRf = "";
        $strTitle_numroomPIm = "";
        if (($objRealty->nRooms > 0) && ($objRealty->enmRealtyCategory != CEnmRealtyCategory::ROOM)) {
            $strTitle = $objRealty->nRooms."-комн.";
            $strTitle_numroom = $objRealty->nRooms."-комн.";
            $strTitle_numroomPIf = $objRealty->nRooms."-комнатная";
            $strTitle_numroomPRf = $objRealty->nRooms."-комнатную";
            $strTitle_numroomPIm = $objRealty->nRooms."-комнатный";
        }

        if (($objRealty->enmRealtyCategory !== NULL) && ($objRealty->enmRealtyCategory !== "")){
            if ($strTitle != "")
                $strTitle .= " ";

            if ($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) {
                $strTitle .= CAllLists::$enmCommerceType[$objRealty->enmCommerceType]->strTextValue;
                $strTitle_emptytxt = CAllLists::$enmCommerceType[$objRealty->enmCommerceType]->strTextValue;
                
            } else {
                $strTitle .= CAllLists::$enmRealtyCategory[$objRealty->enmRealtyCategory]->strTextValue;
                $strTitle_emptytxt = CAllLists::$enmRealtyCategory[$objRealty->enmRealtyCategory]->strTextValue;
            }
        }

        //ЦЕНА
        $start = strlen($objRealty->nPrice)-3;
        $len = strlen($objRealty->nPrice);
        $str = "";
        while ($start > 0){
            $str = substr($objRealty->nPrice, $start, 3) . '&nbsp;'. $str;
            $len -= 3;
            $start -= 3;
        }
        $str_price = substr($objRealty->nPrice, 0, $len) . '&nbsp;' . $str;

        //Валюта
        switch ($objRealty->lstPriceCurrency){
            case CLstPriceCurrency::RUB:
		$strCurrency = '&nbsp;рублей';
                break;
            case CLstPriceCurrency::USD:
                $strCurrency = '&nbsp;$';
                break;
            case CLstPriceCurrency::EUR:
                $strCurrency = '&nbsp;EUR';
                break;
        }
        
        if($objRealty->enmDealType == CEnmDealType::SALE) {
	      //$txtDealType = "Продается"; 
	      //$txtDealTypeS = "Продаются";
	      //переменные задаются в шаблоне
	      $txtDealType = "";
	      $txtDealTypeS = "";
	} else {
	      //$txtDealType = "Сдается"; 
	      //$txtDealTypeS = "Сдаются";
	      $txtDealType = "";
	      $txtDealTypeS = "";
	      $strPricePeriod = "";
	      $strPricePeriod = CAllLists::$enmPricePeriod[$objRealty->enmPricePeriod]->strTextValue;
	}
        
// echo "<pre>";
// print_r($objRealty);
// echo "</pre>";
        

        //Массив с фразами
        $arResult = array();

        //--------------ФРАЗА 0-------------
        //Универсальная фраза
        //$phrase = 'Внимание! Интересное предложение.<br /> '.'<span class="CapLetter">'.$strTitle_emptytxt.'</span>';
        //$phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup><br/> всего за <span class="price">'.$str_price.'</span> '.$strCurrency;
        //$arResult[] = $phrase;
        
        
        
        
        //Квартира
        if($objRealty->enmRealtyCategory == CEnmRealtyCategory::FLAT) {
        
            //Продается
	    if ($objRealty->enmDealType == CEnmDealType::SALE) {
        
		//--------------ФРАЗА 1-------------
		$phrase = " Прекрасная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
		if ($objRealty->nFloor != NULL)
		    $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
		if ($objRealty->nFloorsTotal != NULL)
		    $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома';
		$phrase .= "<br/> всего за&nbsp;<span class=\"price\">".$str_price.'</span>&nbsp;'.$strCurrency.'.';
		$arResult[] = $phrase;
		

		//--------------ФРАЗА 2-------------
		$phrase = " Уютная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
		if ($objRealty->nFloor != NULL)
		    $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
		if ($objRealty->nFloorsTotal != NULL)
		    $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома';
		$phrase .= " за&nbsp;<span class=\"price\">".$str_price.'</span>&nbsp;'.$strCurrency.'. <br /> Эта квартира ждет Вас!';
		$arResult[] = $phrase;
		
		
		//--------------ФРАЗА 3-------------
		$phrase = " Уютная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
		if ($objRealty->nFloor != NULL)
		    $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
		if ($objRealty->nFloorsTotal != NULL)
		    $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома, ';
		$phrase .= " всего за&nbsp;<span class=\"price\">".$str_price.'</span>&nbsp;'.$strCurrency.',<br /> на которую обязательно стоит взглянуть!';
		$arResult[] = $phrase;
		
		
		//--------------ФРАЗА 4-------------
		$phrase = " Замечательная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
		if ($objRealty->nFloor != NULL)
		    $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
		if ($objRealty->nFloorsTotal != NULL)
		    $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома';
		$phrase .= "<br/> Ценность которой гораздо выше ее стоимости в&nbsp;<span class=\"price\">".$str_price.'</span>&nbsp;'.$strCurrency.'!';
		$arResult[] = $phrase;
		
		
		//--------------ФРАЗА 5-------------
		$phrase = " Уютная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt." в которую Вы будете с гордостью звать гостей! ";
		$phrase .= "<br/>Цена в&nbsp;<span class=\"price\">".$str_price.'</span>&nbsp;'.$strCurrency.' гораздо ниже стоимости этой квартиры!';
		$arResult[] = $phrase;
		
		
		//--------------ФРАЗА 6-------------
		//$phrase = " Замечательная ".$strTitle_numroomPIf." ".$strTitle_emptytxt.". <br/>";
		//$phrase .= ' Её ценность которой гораздо выше стоимости! <br/> ';
		//$phrase .= " Не покупайте квартиру, пока не посмотрите эту!";
		//$arResult[] = $phrase;
		
		
		// На эту прекрасную 2-комнатную  квартиру на 3 этаже 5 этажного дома стоимостью всего  ___________ рублей обязательно стоит взглянуть!
		//--------------ФРАЗА 7-------------
		$phrase = " ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
		if ($objRealty->nFloor != NULL)
		    $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
		if ($objRealty->nFloorsTotal != NULL)
		    $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
		$phrase .= '<br/> На эту прекрасную '.$strMore2.' стоимостью всего&nbsp;'.$str_price.'&nbsp;'.$strCurrency;
		$phrase .= '<br /> обязательно стоит взглянуть! ';
		$arResult[] = $phrase;
		
	      
	    }  else {
	
		//Сдается
		// Сдается прекрасная 3-комнатная квартира 50 кв.м. на 2 этаже 5 этажного дома всего за 30000 руб. в месяц
		$arResult = array(); //обнуляем массив чтобы не попала универсальная фраза

		//--------------ФРАЗА 1-------------
		$phrase = " Прекрасная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
		$phrase .= ' '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
		if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
		if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
		$phrase .= ' <br/> всего за&nbsp;'.$str_price.'&nbsp;'.$strCurrency.' в '.$strPricePeriod.'. ';
		$arResult[] = $phrase;
		
	    }
	}
        
        
        //Комната 
        if($objRealty->enmRealtyCategory == CEnmRealtyCategory::ROOM) {
	      
	      if ($objRealty->enmDealType == CEnmDealType::SALE) {
	      //Продается
	      //--------------ФРАЗА 2-------------
	      $phrase = " Уютная $strTitle_emptytxt";
	      if ($objRealty->nFloor != NULL)
		  $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
	      if ($objRealty->nFloorsTotal != NULL)
		  $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома';
	      $phrase .= "<br/> за <span class=\"price\">".$str_price.'</span> '.$strCurrency.'.';
	      $phrase .='<br/>Комната ждет Вас!';
	      $arResult[] = $phrase;
	         
	      
	      } else {
	      
	      //Сдается
	      // Сдается прекрасная 3-комнатная квартира 50 кв.м. на 2 этаже 5 этажного дома всего за 30000 руб. в месяц
	      //--------------ФРАЗА 1-------------
	      $phrase = " Уютная ".$strTitle_numroomPIf.' '.$strTitle_emptytxt;
	      $phrase .= ' '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
	      if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже';
	      if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
	      $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.' в '.$strPricePeriod.'. ';
	      $arResult[] = $phrase;
	      
	      }
	}
        
        
        //Дом 
        //Продается
        if($objRealty->enmRealtyCategory == CEnmRealtyCategory::HOUSE) {
	      
	      // Вы будете с удовольствием звать гостей в этот  4-комнатный  2-этажный дом площадью 100 кв.м. на участке 40 сот. стоимостью всего 50000000 руб.
	      if ($objRealty->enmDealType == CEnmDealType::SALE) {
	      //--------------ФРАЗА 1-------------
	      $phrase = "  ".$strTitle_numroomPIm;
	      if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажный дом';
	      $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
	      if ($objRealty->nAreaLot != NULL) $phrase .= 'на участке '.$objRealty->nAreaLot.' сот. ';
	      $phrase .= ' <br/> стоимостью всего '.$str_price.' '.$strCurrency.'! ';
	      $phrase .= "<br />В этот дом Вы с удовольствием будете звать гостей!";
          $arResult[] = $phrase;
	      
	      
	      } else {
	      
	      // Сдается замечательный 5 комнатный 1 этажный дом площадью 120 кв.м. на участке 3 сот. Всего за 20000 руб. в месяц
	      //--------------ФРАЗА 1-------------
	      $phrase = " Замечательный ".$strTitle_numroomPIm;
	      if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажный дом';
	      $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
	      if ($objRealty->nAreaLot != NULL) $phrase .= 'на участке '.$objRealty->nAreaLot.' сот. ';
	      $phrase .= ' <br/> Всего за '.$str_price.' '.$strCurrency.' в '.$strPricePeriod.'. ';
	      $arResult[] = $phrase;
	      }
	}
        
        
        
        //Таунхаус 
        if ($objRealty->enmRealtyCategory == CEnmRealtyCategory::TOWNHOUSE) {
        //--------------ФРАЗА 1-------------
        //Продается замечательный 4 комнатный 1 этажный таунхаус площадью 100 кв.м. на участке 2 сот.  всего за 6 000 000 руб.
        $phrase = " Замечательный ".$strTitle_numroomPIm;
        if ($objRealty->nFloorsTownhouse != NULL) $phrase .= ' '.$objRealty->nFloorsTownhouse.'-этажный ';
        $phrase .= ' '.$strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nAreaLot != NULL) $phrase .= 'на участке '.$objRealty->nAreaLot.' сот. ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Земельный участок 
        if ($objRealty->enmRealtyCategory == CEnmRealtyCategory::LAND) {
        //--------------ФРАЗА 1-------------
        //Продается прекрасный земельный участок площадью 3.6 сот. всего за 5 000 000 руб.
        $phrase = " Прекрасный земельный участок ";
        if ($objRealty->nAreaLot != NULL) $phrase .= ' площадью '.$objRealty->nAreaLot.' сот. ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Автосервис 
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 0)) {
        //--------------ФРАЗА 1-------------
        //Продается замечательный автосервис площадью 100 кв.м. на 1 этаже 5 этажного дома всего за 5000 000 руб.
        $phrase = " Замечательный ".$strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Готовый бизнес 
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 1)) {
        //--------------ФРАЗА 1-------------
        //Продается выгодный готовый бизнес площадью 12 кв.м. на 2 этаже 7 этажного дома всего за 4 000 0000 руб.
        $phrase = " Выгодный ".$strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Помещения свободного назначения
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 2)) {
        //--------------ФРАЗА 1-------------
        //Продаются отличные помещения свободного назначения площадью 30 кв.м. на 8 этаже 9 этажного дома всего за 700000000 руб.
        // $strTitle_emptytxt содержит "помещения свободного назначеня" - множественное число
        $phrase = " Отличное помещение свободного назначения ";
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Земли коммерческого назначения
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 3)) {
        //--------------ФРАЗА 1-------------
        //Продаются замечательные земли коммерческого назначения площадью 20 сот. всего за 700000000 руб.
        //$strTitle_emptytxt содержит "земли коммерческого назначения" - множественное число
        $phrase = " Замечательный участок коммерческого назначения ";
        if ($objRealty->nAreaLot != NULL) $phrase .= ' площадью '.$objRealty->nAreaLot.' сот. ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        //Коммерческая - Юридический адрес
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 6)) {
        //--------------ФРАЗА 1-------------
        //Продается юридический адрес площадью 12 кв.м. на 2 этаже 7 этажного дома всего за 40000000 руб.
        $phrase = " Юридический адрес ";
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Производственное помещение
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 7)) {
        //--------------ФРАЗА 1-------------
        //Продается замечательное производственное помещение площадью 100 кв.м. на 1 этаже 3 этажного дома  всего за 6000000 руб.
        $phrase = " Замечательное ".$strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Офис
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 8)) {
        //--------------ФРАЗА 1-------------
        //Продается замечательный 4 комнатный офис площадью 100 кв.м. на 21 этаже 45 этажного дома  всего за 6000000 руб.
        $phrase = " Замечательный ";
        $phrase .= $strTitle_numroomPIm.' '.$strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Общепит
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 9)) {
        //--------------ФРАЗА 1-------------
        //Продается замечательный общепит площадью 100 кв.м. на 1 этаже 5 этажного дома всего за 5000 000 руб.
        $phrase = " Замечательный ";
        $phrase .= $strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Торговые помещения
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 10)) {
        //--------------ФРАЗА 1-------------
        //Продаются отличные торговые помещения  площадью 30 кв.м. на 8 этаже 9 этажного дома всего за 700000000 руб.
        //$strTitle_emptytxt содержит "торговые помещения" - множественное число
        $phrase = " Отличное торговое помещение";
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Склад
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 11)) {
        //--------------ФРАЗА 1-------------
        //Продается отличный склад площадью 100 кв.м. на 1 этаже 5 этажного дома всего за 5000 000 руб.
        $phrase = " Отличный ";
        $phrase .= $strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nFloor != NULL) $phrase .= ' на '.$objRealty->nFloor.'-м этаже ';
        if ($objRealty->nFloorsTotal != NULL) $phrase .= ' '.$objRealty->nFloorsTotal.'-этажного дома ';
        $phrase .= ' <br/> всего за '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        //Коммерческая - Гостиница
        if (($objRealty->enmRealtyCategory == CEnmRealtyCategory::COMMERCE) && ($objRealty->enmCommerceType == 12)) {
        //--------------ФРАЗА 0-------------
        //Продается отличная 4-комнатная  2-этажная гостиница площадью 100 кв.м. на участке 40 сот. стоимостью всего 50000000 руб.
        $phrase = " Отличная ";
        if ($objRealty->nFloor != NULL) $phrase .= ' '.$objRealty->nFloor.'-этажная ';
        $phrase .= ' '.$strTitle_emptytxt.' ';
        $phrase .= ' площадью '.$objRealty->nAreaTotal.'м<sup>2</sup> ';
        if ($objRealty->nAreaLot != NULL) $phrase .= ' на участке '.$objRealty->nAreaLot.' сот. ';
        $phrase .= ' <br/> стоимостью всего '.$str_price.' '.$strCurrency.'! ';
        $arResult[] = $phrase;
        }
        
        
        
        
        

        return $arResult;
    }
}


/*
<!-- Не покупайте пока не посмотрите эту -->
<-- Надежное будущее - вложения в собственную недвижимость! -->
<!--Ценность этого уютного (замечательного) 4-комнатного дома на участке 17 соток гораздо выше его стоимости в <span class="price">--><?//=$str_price?><!--</span> рублей!-->

<!--Вы будете с гордостью звать гостей в этот уютный 4-комнатный дом на участке 40 соток ! 

<!-- стоимости этого дома Гораздо ниже чем его цена в <span class="price">--><?//=$str_price?><!--</span> руб-->

<!--Прекрасный 5-комнатный дом на участке 3 сотки  всего за <span class="price">--><?//=$str_price?><!--</span> рублей ждет Вас в качестве нового хозяина!-->

<!--Эта великолепная  2-комнатная дача на участке 2 сотки будет вашей надежной крепостью на долгие годы всего за <span class="price">--><?//=$str_price?><!--</span> рублей!-->

*/
?>