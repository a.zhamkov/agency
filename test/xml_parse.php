<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Test");
$IBLOCK_ID = 2;
$PROP = [];
$arMorePhoto =[];

CModule::IncludeModule('iblock');

function xmlToArray($xml, $options = array()) {
    $defaults = array(
        'namespaceSeparator' => ':',//you may want this to be something other than a colon
        'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
        'alwaysArray' => array(),   //array of xml tag names which should always become arrays
        'autoArray' => true,        //only create arrays for tags which appear more than once
        'textContent' => '$',       //key used for the text content of elements
        'autoText' => true,         //skip textContent key if node has no attributes or child nodes
        'keySearch' => false,       //optional search and replace on tag and attribute names
        'keyReplace' => false       //replace values for above search values (as passed to str_replace())
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = null; //add base (empty) namespace

    //get attributes from all namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
            //replace characters in attribute name
            if ($options['keySearch']) $attributeName =
                str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
            $attributeKey = $options['attributePrefix']
                . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                . $attributeName;
            $attributesArray[$attributeKey] = (string)$attribute;
        }
    }

    //get child nodes from all namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->children($namespace) as $childXml) {
            //recurse into child nodes
            $childArray = xmlToArray($childXml, $options);
            list($childTagName, $childProperties) = each($childArray);

            //replace characters in tag name
            if ($options['keySearch']) $childTagName =
                str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
            //add namespace prefix, if any
            if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

            if (!isset($tagsArray[$childTagName])) {
                //only entry with this key
                //test if tags of this type should always be arrays, no matter the element count
                $tagsArray[$childTagName] =
                    in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                        ? array($childProperties) : $childProperties;
            } elseif (
                is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                === range(0, count($tagsArray[$childTagName]) - 1)
            ) {
                //key already exists and is integer indexed array
                $tagsArray[$childTagName][] = $childProperties;
            } else {
                //key exists so convert to integer indexed array with previous value in position 0
                $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
            }
        }
    }

    //get text content of node
    $textContentArray = array();
    $plainText = trim((string)$xml);
    if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

    //stick it all together
    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
        ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

    //return node as array
    return array(
        $xml->getName() => $propertiesArray
    );
}

function propsToList($iblockId) {
    $prop_list = [];
    $arFilter = array(
        'IBLOCK_ID' => $iblockId
    );
    $rsProperty = CIBlockProperty::GetList(
        array(),
        $arFilter
    );
    while($element = $rsProperty->Fetch())
    {
        $prop_list[$element['ID']]['ID'] = $element['ID'];
        $prop_list[$element['ID']]['NAME'] = $element['NAME'];
        $prop_list[$element['ID']]['SORT'] = $element['SORT'];
        $prop_list[$element['ID']]['CODE'] = $element['CODE'];
        $prop_list[$element['ID']]['PROPERTY_TYPE'] = $element['PROPERTY_TYPE'];
        $prop_list[$element['ID']]['MULTIPLE'] = $element['MULTIPLE'];
    }
    return $prop_list;
}

function propList($iblockId, $propCode, $propValue) {
    $property_enums = CIBlockPropertyEnum::GetList(Array(),
        Array(
            "IBLOCK_ID"=>$iblockId,
            "CODE"=>$propCode
        )
    );
    while($enum_fields = $property_enums->GetNext())
    {
        echo $enum_fields['ID']." - ".$enum_fields['VALUE']."<br>";
        echo "value - " . $propValue . "<br>";
    }
}

function multiKeyExists(array $arr, $key) {
    // is in base array?
    if (array_key_exists($key, $arr)) {
        if (is_array($arr[$key])) {
            return $arr[$key]['value'];
        } else {
            return $arr[$key];
        }
    } else {
        foreach ($arr as $element) {
            if (is_array($element)) {
                $result = multiKeyExists($element, $key);
                if ($result != null) {
                    return $result;
                }
            }
        }
    }
    return null;
}

function singleMultiKeyExists(array $arr, $key) {
    // is in base array?
    if (array_key_exists($key, $arr)) {
        if (is_array($arr[$key])) {
            return $arr[$key]['value'];
        } else {
            return $arr[$key];
        }
    } else {
        foreach ($arr as $element) {
            if (is_array($element)) {
                $result =  singleMultiKeyExists($element, $key);
                if ($result != null) {
                    return $result;
                }
            }
        }
    }
    return null;
}

function addPropTypeList($iblockId, $propCode, $propValue) {
    $ENUM_ID = 0;
    $property_enums = CIBlockPropertyEnum::GetList(Array(), Array("IBLOCK_ID"=>$iblockId, "CODE"=>$propCode, "VALUE" => $propValue));
    while($enum_fields = $property_enums->GetNext())
    {
        $ENUM_ID =  $enum_fields["ID"];
//        echo 'enum - ' . $ENUM_ID;
    }
    return $ENUM_ID;
}

function addPropTypeFile ($propCode, array $images) {
    // ****** MORE_PHOTO*******
    $rsMorePhoto = [];
    $i = 0;
    foreach ($images as $image) {
        $rsMorePhoto['n'.$i] = array("VALUE"=>CFile::MakeFileArray($image));
        $i++;
    }
    // *************************
    return $rsMorePhoto;
}

function findIdElementFromCode ($code) {

}

function validateCode ($code) {
    $str = mb_strtolower($code);
    $strFirst = mb_strcut($str, 0,1);
    if ($strFirst == '_') {
        $str = substr_replace($str, '@', 0, 1);
    }
    $str = str_ireplace("_", "-", $str);
    return $str;
}
//$feed = simplexml_load_file("feed_full.xml"); //

//$data = xmlToArray($feed); // массив из фида

$el = new CIBlockElement;

$propList = propsToList($IBLOCK_ID); // список свойств инфоблока

//$value = singleMultiKeyExists($data['realty-feed']['offer'][0], 'name');
//echo 'value: ' . $value . '</br>';

foreach ($data['realty-feed']['offer'] as $offer) {

    if (!empty($offer['category'])) {
        $strLoc = $offer['area']['value'] . ' м2, '
                . $offer['location']['address'] . ', '
                . $offer['location']['locality-name'];
        switch ($offer['category']) {
            case "flat":
                if($offer['rooms']){
                    $strRomms = $offer['rooms'] . '-комн.' .' квартира,';
                    $name = $strRomms . $strLoc;
                } else if($offer['studio']) {
                    $strStudio = 'квартира-студия ';
                    $name = $strStudio . $strLoc;
                } else {
                    $name = $strLoc;
                }
                break;
            default:
                echo 'пустой тип';
        }
    } else {
        echo 'пустой тип';
        $name = $offer['@internal-id'];
    }

    foreach ($propList as $prop) {
        $code = validateCode($prop["CODE"]);
        $propValue = multiKeyExists($offer, $code); // ищем значение свойства в элементе фида
        if($propValue) {
            switch ($prop["PROPERTY_TYPE"]) {

                case "N": // свойство NUMBER
                    $PROP[$prop["CODE"]] = $propValue;
                    break;
                case "L": //LIST
                    $ENUM_ID = addPropTypeList($IBLOCK_ID, $prop["CODE"], $propValue);
                    $PROP[$prop["CODE"]] = array("VALUE" => $ENUM_ID);
                    break;
                case "S": // STRING
                    $PROP[$prop["CODE"]] = $propValue;
                    break;
//                case "F": // FILE
//                    $arMorePhoto = addPropTypeFile($prop["CODE"], $offer['image']);
//                    //$morePhoto['MORE_PHOTO'] = $arMorePhoto;
//                    echo 'свойство файл ' . $prop["CODE"] . 'значение - ' . $propValue .'<br/>';
//                    break;
                default:
                    break;
//                    echo 'ошибка нет собвадений типа для свойства' .'<br/>';
            }
        } else {
            switch ($prop["PROPERTY_TYPE"]) {
                case "F": // свойство файл
                    $arMorePhoto = addPropTypeFile($prop["CODE"], $offer['image']);
                    break;
                default:
                    break;
//                    echo 'ошибка нет собвадений типа для свойства' . '<br/>';
            }
        }
    }

//  формируем основные поля элемента
    $description = $offer['description'];

    // находим ИД элемента по символьному коду
    $params = Array(
        "max_len" => "100", // обрезает символьный код до 100 символов
        "change_case" => "L", // буквы преобразуются к нижнему регистру
        "replace_space" => "_", // меняем пробелы на нижнее подчеркивание
        "replace_other" => "_", // меняем левые символы на нижнее подчеркивание
        "delete_repeat_replace" => "true", // удаляем повторяющиеся нижние подчеркивания
        "use_google" => "false", // отключаем использование google
    );
    $PROP['MORE_PHOTO'] = $arMorePhoto;

    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID" => false,
        "IBLOCK_ID" => $IBLOCK_ID,
        "NAME" => $name,
        "CODE" => $offer['@internal-id'],
        "ACTIVE" => "Y",
        "PROPERTY_VALUES" => $PROP,
        "DETAIL_TEXT" => $description,
    );

    $PRODUCT_ID = CIBlockFindTools::GetElementID(false, $arLoadProductArray["CODE"], false, false, array("IBLOCK_ID" => '2'));

    if($PRODUCT_ID) {
        unset($arLoadProductArray["IBLOCK_ID"]);
//        CIBlockElement::SetPropertyValues($PRODUCT_ID, $IBLOCK_ID, array("PROPERTY_VALUES" => $PROP), false);
        CIBlockElement::SetPropertyValuesEx($PRODUCT_ID, $IBLOCK_ID, array('7' => Array ("VALUE" => array("del" => "Y"))));

        $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
        echo "UPdate ID: " . $arLoadProductArray["CODE"] .'<br/>';
    } else {
        if ($ID = $el->Add($arLoadProductArray)) {
            echo "New ID: " . $ID .'<br/>';
        }else {
            echo "Error: " . $el->LAST_ERROR .'<br/>';

        }

    }

}

function outputLines ($tmp) // вывод полей
{
    $data = [];
    $dataIn = [];
    $dataList = [];
    $dataLoc = [];
    $dataListLoc = [];
    foreach ($tmp['realty-feed']['offer'] as $offer) {

        //    ************** вывод значений ключа
        foreach ($offer as $key => $value) {
            if ($key === 'location') {
                $data[$value] = $value;
                if (array_key_exists($value, $data)) {
                    $data[$value] = $value;
                }
            }
        }
        //      ************

        //    ************** вывод значений ключа
        foreach ($offer['location'] as $key => $value) {
            if ($key === 'district') {
                $dataLoc[$value] = $value;
                if (array_key_exists($value, $data)) {
                    $dataLoc[$value] = $value;
                }
            }
        }
        //      ************

        //    ************** вывод значений ключа с вложением
        foreach ($offer as $elem) {
            if (is_array($elem)) {
                foreach ($elem as $key => $value) {
                    //                if ($key === 'location') {
                    $dataIn[$value] = $value;
                    if (array_key_exists($value, $data)) {
                        $dataIn[$value] = $value;
                    }
                    //                }
                }
            }

        }
        //      ************

        //    ******************* вывод ключей
        $dataKeys = array_keys($offer);
        foreach ($dataKeys as $key => $value) {
            $dataList[$value] = $value;
            if (array_key_exists($value, $dataKeys)) {
                $dataList[$value] = $value;
            }
        }
        //    ******************

        //    ******************* вывод ключей вложений
        $dataKeysLoc = array_keys($offer['location']);
        foreach ($dataKeysLoc as $k => $value) {
            $dataListLoc[$value] = $value;
            if (array_key_exists($value, $dataKeys)) {
                $dataListLoc[$value] = $value;
            }
        }

        //    ********************

    }

}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
